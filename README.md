> # Open API documentation

      v1 2023 / 1 / 3





> #### 目錄

[TOC]



# 前言



## 加密方式

**Header 與 Body 皆使用 JWT 做 verify**

1. Header - JWT token 可以從AGLogin的access token獲取
2. POST 的 Body 在傳輸的時候會給代理商 JWT 的 secret 作為資料傳輸的加密金鑰， 將要傳 JSON 的 Body 加密後傳輸



## 代理驗證流程
![](/Users/abbie_lin/Desktop/截圖 2020-04-24 上午11.33.58.png)

1. 打 AGLogin API 正確取得 access token後
2. token 存到 http header Authorization Bear {{token}}
3. 在其他需要驗證代理的 API 都需攜帶 token 在 Header 中


## 使用者登入遊戲流程
![](/Users/abbie_lin/Desktop/截圖 2020-04-24 上午11.34.04.png)

1. 打 AGLogin API 正確取得access token後

2. token存到 Authorization Bear {{token}}

3. 打 LaunchGame API攜帶相關參數

4. 返回的 url 即為使用者登入的 url



## API 請求規範

皆採取 POST 方式請求，請求 Body 的規範第一層固定是：

```json
{
	  "Request": 內含創建、讀取請求的資料,
    "Action": 指定哪一隻API, 為 API 名稱 ex: "AGLogin"
}
```


## API 回傳規格

回傳的token解密完後，payload的訊息第一層固定為 `RespCode`, `Status`, `Action`, `Data`.

1. RespCode 對應附件的Response & Errcode對應表格
2. Status 成功success, 失敗fail
3. Action 為對應request的Action
4. Data 內含回傳的相關資料

```json
{
    "RespCode": "5000001",
    "Status": "fail",
    "Action": "KickOutMem",
    "Data": {
        "username": "testMember01",
        "userid": "1234567"
    }
}
```




# 1. AGLogin 代理登入拿到access token

```javascript
POST {{api_server_url}}/v1
```

## Request
| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明     |
| -------- | -------- | -------- | ---- | ------------ |
| username | body     | string   | Y    | 代理帳戶名稱 |
| password | body     | string   | Y    | 代理密碼     |

**Request in body format**

```json
{
	"Request": {
		"username": "testAgent01",
		"password": "thisispassword"
	},
	"Action": "AGLogin"
}
```

## Response

| 參數名稱    | 參數種類 | 參數型態 | 必填 | 參數說明                                       |
| ----------- | -------- | -------- | ---- | ---------------------------------------------- |
| accessToken | body     | string   | Y    | 為之後代理需要帶在Authroization的 access token |

**Response 200 OK**

```JSON
{
	"RespCode": "2000001",
	"Status": "success",
	"Action": "AGLogin",
    "Data": {
        "accessToken": "jsonwebtoken.string"
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "AGLogin",
    "Data": {
        "accessToken": null
    }
}
```



# 2. CreateMember 創建使用者
```javascript
POST {{api_server_url}}/v1
```
## Request

| 參數名稱    | 參數種類 | 參數型態 | 必填 | 參數說明     |
| ----------- | -------- | -------- | ---- | ------------ |
| username    | body     | string   | Y    | 會員帳戶     |
| rawEcId     | body     | string   | Y    | 品牌id       |
| rawEcName   | body     | string   | Y    | 品牌代號     |
| rawEcUserId | body     | string   | Y    | 品牌使用者id |

**Request in body format**

```json
{
	"Request": {
      "username": "testAgent01",
      "rawEcId": "13344",
      "rawEcName": "AG",
      "rawEcUserId": "AG13345",
	},
	"Action": "CreateMember"
}
```


## Response

| 參數名稱   | 參數種類 | 參數型態 | 必填 | 參數說明                 |
| ---------- | -------- | -------- | ---- | ------------------------ |
| userid     | body     | string   | Y    | 會員id                   |
| username   | body     | string   | Y    | 會員帳戶                 |
| userenable | body     | string   | Y    | 會員激活狀態(y:是, n:否) |
| balance    | body     | string   | Y    | 會員餘額(RMB)            |

**Response 200 OK**

```JSON
{
	"RespCode": "2000001",
	"Status": "success",
	"Action": "CreateMember",
    "Data": {
        "userdata": {
            "userid": "920020",
            "username": "test23",
            "userenable": "Y",
            "balance": "0"
        }

```
**Response 4040099 DUPLICATE_USER 使用者重複**

```json
{
	"RespCode": "4040099",
	"Status": "fail",
	"Action": "CreateMember",
    "Data": {
        "userdata": {
            "userid": "null",
            "username": "null",
            "userenable": "null",
            "balance": "null"
        }
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "CreateMember",
    "Data": {
        "userdata": {
            "userid": "null",
            "username": "null",
            "userenable": "null",
            "balance": "null"
        }
    }
}
```



# 3. GetGameList 獲取遊戲列表

```javascript
POST {{api_server_url}}/v1
```

## Request
| 參數名稱 | 參數種類 | 參數型態 | 必填   | 參數說明                                  |
| -------- | -------- | -------- | ------ | ----------------------------------------- |
| page     | body     | string   | Y      | 頁數                                      |
| limit    | body     | string   | option | 一頁有幾筆                                |
| langx    | body     | string   | Y      | 語言 為以下三種 ‘en-us', 'zh-tw', 'zh-cn' |

**Request in body format**

```json
{
	"Request":  {
      "page": "1",
      "limits": "15",
      "langx": "zh-cn"

  },
	"Action": "GetGameList"
}
```



## Response

| 參數名稱  | 參數種類 | 參數型態 | 必填 | 參數說明 |
| --------- | -------- | -------- | ---- | -------- |
| totalpage | body     | string   | Y    | 總頁數   |
| page      | body     | string   | Y    | 第幾頁   |
| gamelist  | body     | array    | Y    | 遊戲列表 |

**gamelist 內的參數說明**

| 參數名稱         | 參數種類 | 參數型態 | 必填 | 參數說明                                     |
| ---------------- | -------- | -------- | ---- | -------------------------------------------- |
| gameid           | body     | string   | Y    | 遊戲編號                                     |
| gamename         | body     | string   | Y    | 遊戲名稱                                     |
| gametad          | body     | string   | Y    | 遊戲標籤 0: 熱門遊戲  1: 推薦遊戲  2: 新遊戲 |
| gamestatus       | body     | string   | Y    | 遊戲狀態 0: 營運中   -1: 維護中              |
| gameicon         | body     | string   | Y    | 遊戲圖標 大小: 540px 360px                   |
| gameicon2        | body     | string   | Y    | 遊戲圖標 大小: 208px 208px                   |
| gameruleurl      | body     | string   | Y    | 遊戲規則網頁                                 |
| version          | body     | string   | Y    | 遊戲版號                                     |
| downloadlocation | body     | string   | Y    | 下載位置                                     |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Action": "GetGameList",
    "Data": {
        "totalpage": "1",
        "page": "1",
        "gamelist": [
            {
                "gameid": "18",
                "gamename": "搶庄牛牛",
                "gamepopular": "[0]",
                "gamestate": "0",
                "gameicon": "4ab2a9ca0c5ab650cf8eb717204353c3",
                "gameicon2": "4ab2a9ca0c5ab650cf8eb717204353c3",
                "gameruleurl": "/games/g18",
              	"version": "1.0.0",
              	"downloadlocation": "www.google.com"
            }
        ]
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "GetGameList",
    "Data": {
        "gamelist": [{
    		"gameid": "null",
            "gamename": "null",
            "gamepopular": "null",
            "gamestate": "null",
            "gameicon": "null",
            "gameicon2": "null",
            "gameruleurl": "null",
            "gameplatform": "null",
          	"version": "null",
            "downloadlocation": "null"
        }]
    }
}
```



# 4. CheckMemberBalance 查詢玩家餘額
```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| username | body     | string   | Y    | 會員名稱 |

**Request in body format**

```json
{
	"Request":  {
  		"username": "test1",
  },
	"Action": "CheckMemberBalance"
}
```



## Response

| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| username | body     | string   | Y    | 會員名稱 |
| userid   | body     | string   | Y    | 會員編號 |
| balance  | body     | string   | Y    | 會員餘額 |
| currency | body     | string   | Y    | 貨幣單位 |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Action": "CheckMemberBalance",
    "Data": {
        "balance": "1.25e+87",
        "currency": "RMB",
        "username": "test1",
        "userid": "34867"
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "CheckMemberBalance",
    "Data": {
		"balance": "null",
        "currency": "null",
        "username": "null",
        "userid": "null"
    }
}
```



# 5. GetTransactionsRecords 獲取轉帳紀錄

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                             |
| -------------- | -------- | -------- | ------ | ------------------------------------ |
| starttimestamp | body     | string   | Y      | 開始時間                             |
| endtimestamp   | body     | string   | Y      | 結束時間                             |
| page           | body     | string   | Y      | 第幾頁                               |
| username       | body     | string   | option | 會員名稱                             |
| limits         | body     | string   | option | 每頁有幾筆，沒有帶參數的話預設 25 筆 |

**Request in body format**
- 查詢時間區段內的所有轉帳記錄

```json
{
	"Request":  {
      "starttimestamp": "1586422527681",
      "endtimestamp": "1587030250328",
      "page": "1",
  },
	"Action": "GetTransactionsRecords"
}
```
- 查詢時間區段內某個會員轉帳記錄

```json
{
	"Request":  {
    	"username": "test1",
      "starttimestamp": "1586422527681",
      "endtimestamp": "1587030250328",
      "page": "1",
  },
	"Action": "GetTransactionsRecords"
}
```



## Response

| 參數名稱    | 參數種類 | 參數型態 | 必填 | 參數說明            |
| ----------- | -------- | -------- | ---- | ------------------- |
| transuserid | body     | string   | Y    | 會員轉帳唯一序號    |
| transrecid  | body     | string   | Y    | 交易回饋的紀錄 id   |
| transpayno  | body     | string   | Y    | 存提交易時定義的 id |
| transamount | body     | string   | Y    | 交易金額            |
| transdate   | body     | string   | Y    | 交易時間            |
| transpayway | body     | string   | Y    | 存款"1",提款"-1"    |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "transdata": [
            {
                "transuserid": "34867",
                "transrecid": "1043279",
                "transpayno": "34863-3",
                "transamount": "125",
                "transdate": "1586423614639",
                "transpayway": "1"
            },
        ]
    },
    "Action": "GetTransactionsRecords"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Data": {
        "transdata": [
            {
                "transuserid": "null",
                "transrecid": "null",
                "transpayno": "null",
                "transamount": "null",
                "transdate": "null",
                "transpayway": "null"
            },
        ]
    },
    "Action": "GetTransactionsRecords"
}
```

# 6. CheckWagerRecords 查詢注單列表

```javascript
POST {{api_server_url}}/v1
```

*** 本接口 30s 只允许调用一次，每次最大返回 1000 条数据。***

## Request


| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                                                     |
| -------------- | -------- | -------- | ------ | ------------------------------------------------------------ |
| username       | body     | string   | option | 會員編號 帶 username 為取該會員注單列表，不帶則取得所有會員注單列表 |
| starttimestamp | body     | string   | Y      | 搜尋開始時間                                                 |
| endtimestamp   | body     | string   | Y      | 搜尋結束時間                                                 |
| page           | body     | string   | Y      | 頁數                                                         |
| langx          | body     | string   | Y      | 語言 為以下三種 'en-us', 'zh-tw', 'zh-cn'                    |

**Request in body format**

- 查詢所有會員注單列表

```json
{
	"Request":  {
                    "starttimestamp": "1585635151793",
                    "endtimestamp": "1585635602099",
                    "page": "1",
                    "langx": "zh-cn"
                },
	"Action": "CheckWagerRecords"
}
```

- 查詢單一會員注單列表

```json
{
	"Request":  {
                    "username": "test1",
                    "starttimestamp": "1585635151793",
                    "endtimestamp": "1585635602099",
                    "page": "1",
                    "langx": "zh-cn"
                },
	"Action": "CheckWagerRecords"
}
```



## Response

| 參數名稱  | 參數種類 | 參數型態 | 必填 | 參數說明 |
| --------- | -------- | -------- | ---- | -------- |
| totalpage | body     | string   | Y    | 總頁數   |
| page      | body     | string   | Y    | 第幾頁   |
| wagerdata | body     | object   | Y    | 注單資料 |

**wagerdata 內的參數說明**

| 參數名稱         | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| ---------------- | -------- | -------- | ---- | ------------------------------------------------------------ |
| wagerid          | body     | string   | Y    | 注單單號                                                     |
| gamerecordid     | body     | string   | Y    | 局號                                                         |
| username         | body     | string   | Y    | 會員名稱                                                     |
| userid           | body     | string   | Y    | 會員編號                                                     |
| ecsiteid         | body     | string   | Y    | 代理商編號                                                   |
| gameid           | body     | string   | Y    | 遊戲編號                                                     |
| beforebetbalance | body     | string   | Y    | 投注前金額                                                   |
| betamount        | body     | string   | Y    | 下注金額                                                     |
| validamount      | body     | string   | Y    | 有效下注金額                                                 |
| winamount        | body     | string   | Y    | 派彩金額，遊戲方回給玩家金額+房主返水                        |
| resultamount     | body     | string   | Y    | 損益金額+房主返水                                            |
| settle           | body     | string   | Y    | '1'：有結果, '0':沒結果                                      |
| result           | body     | string   | Y    | 注單結果 "L": 輸, "W": 贏, "D": 取消, "N": 打平, "0" 沒結果  |
| afterbetbalance  | body     | string   | Y    | 投注後金額                                                   |
| wagerdate        | body     | string   | Y    | 注單日期                                                     |
| device           | body     | string   | Y    | 下注裝置 手機"M"，網頁"S"                                    |
| gametype         | body     | string   | Y    | 麻將遊戲類型 1: 單人; 2: 多人; 3: 多人對戰; 4: 好友房 沒有是'null' |
| kkBeforBalance   | body     | string   | Y    | KK錢包扣款前餘額                                             |
| kkAfterBalance   | body     | string   | Y    | KK錢包扣款後餘額                                             |
| recordtype       | body     | string   | Y    | 資料列類型 Game: 遊戲紀錄, CompetitionReward: 比賽獎金, CompetitionOrder: 比賽報名費 |


注意: 

* 好友房相關資料時 
    * winamount 會加入房主返水
    * resultamount 會加入房主返水
* 比賽場相關資料時
    *  沒有 gameRecordId, beforeBetBalance, afterBetBalance, validamount 等值
    *  betAmount 為入場費
    *  winAmount 為 獎金或是返還入場費
    *  recordtype 為比賽報名費、比賽獎金時的 wagerid 內容為比賽訂單相關的 Unique Id

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "totalpage": "1",
        "page": "1",
        "wagerdata": [
            {
                "wagerid": "0EV00000000000000067",
              	"gamerecordid": "GR-17202885CCD-1-36",
                "username": "test2",
                "userid": "20200438202",
                "ecsiteid": "34863",
                "gameid": "18",
                "beforebetbalance": "980100",
                "betamount": "100",
                "validamount": "100",
                "winamount": "0",
                "resultamount": "-100",
                "settle": "1",
                "result": "L",
                "afterbetbalance": "980000",
                "wagerdate": "1587464892762",
                "device": "S",
                "gametype": "0",
                "kkBeforBalance": "99900",
                "kkAfterBalance": "100000",
                "recordtype": "Game"
            },
            {
                "wagerid": "0EV00000000000000065",
              	"gamerecordid": "GR-1720D59EC16-2-B0",
                "username": "test2",
                "userid": "20200438202",
                "ecsiteid": "34863",
                "gameid": "18",
                "beforebetbalance": "980300",
                "betamount": "100",
                "validamount": "100",
                "winamount": "0",
                "resultamount": "-100",
                "settle": "1",
                "result": "L",
                "afterbetbalance": "980200",
                "wagerdate": "1587464892760",
                "device": "S",
                "gametype": "3",
                "kkBeforBalance": "99900",
                "kkAfterBalance": "100000",
                "recordtype": "Game"
            },
        ]
    },
    "Action": "CheckWagerRecords"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Data": {
        "totalpage": "1",
        "page": "1",
        "wagerdata": [
            {
                "wagerid": "null",
              	"gamerecordid": "null",
                "username": "null",
                "userid": "null",
                "ecsiteid": "null",
                "gameid": "null",
                "beforebetbalance": "null",
                "betamount": "null",
                "validamount": "null",
                "winamount": "null",
                "resultamount": "null",
                "settle": "null",
                "result": "null",
                "afterbetbalance": "null",
                "wagerdate": "null",
                "device": "null",
                "kkBeforBalance": "null",
                "kkAfterBalance": "null",
                "recordtype": "null",
            },
        ]
    },
    "Action": "CheckWagerRecords"
}
```

# 7. CheckWagerRecordDetail 查詢注單詳細資料

```javascript
POST {{api_server_url}}/v1
```

*** 本接口 30s 只允许调用一次，每次最大返回 1000 条数据。***

| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| wagerid  | body     | string   | Y    | 注單編號 |

**Request in body format**

```json
{
	"Request":  {
			"wagerid": "0E7000000002F0HM0004"
  },
	"Action": "CheckWagerRecordDetail"
}
```



## Response

| 參數名稱        | 參數種類 | 參數型態 | 必填 | 參數說明         |
| --------------- | -------- | -------- | ---- | ---------------- |
| wagerdetaildata | body     | object   | Y    | 注單詳細資料     |
| gamerecorddata  | body     | object   | Y    | 該注單的遊戲紀錄 |

**wagerdetaildata 內的參數說明**

| 參數名稱     | 參數種類 | 參數型態 | 必填 | 參數說明                                          |
| ------------ | -------- | -------- | ---- | ------------------------------------------------- |
| gamerecordid | body     | string   | Y    | 遊戲紀錄編號                                      |
| ecsiteid     | body     | string   | Y    | 代理商編號                                        |
| userid       | body     | string   | Y    | 會員編號                                          |
| gameid       | body     | string   | Y    | 遊戲編號                                          |
| bettimestamp | body     | string   | Y    | 下注時間                                          |
| betamount    | body     | string   | Y    | 下注金額                                          |
| validamount  | body     | string   | Y    | 下注有效金額                                      |
| winamount    | body     | string   | Y    | 下注金額結果+房主返水(好友房)                     |
| result       | body     | string   | Y    | 下注結果L: 輸、W: 贏、D: 取消、N: 沒結果、0: 打平 |
| balance      | body     | string   | Y    | 餘額                                              |

**gamerecorddata 內的參數說明**

| 參數名稱           | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| ------------------ | -------- | -------- | ---- | ------------------------------------------------------------ |
| wagerid            | body     | string   | Y    | 注單編號                                                     |
| gameid             | body     | string   | Y    | 遊戲編號                                                     |
| themeid            | body     | string   | Y    | 廳館編號                                                     |
| roomid             | body     | string   | Y    | 房間編號                                                     |
| gametype           | body     | string   | Y    | 遊戲類型 1: 單人; 2: 多人; 3: 多人對戰; 4: 好友房 沒有是'null' |
| mergedgamerecordid | body     | string   | Y    | 好友房場次號, 一般遊戲是'null'                               |
| starttimestamp     | body     | string   | Y    | 下注開始時間 Unix Epoch timestamp in milliseconds            |
| endtimestamp       | body     | string   | Y    | 下注結束時間 Unix Epoch timestamp in milliseconds            |
| history            | body     | string   | Y    | 遊戲定義的結果，以JSON格式顯示。詳細說明在附件               |


注意: 
大眾&紅中 history 中 胡牌只會有一張所以 winnerTile 為一整數 (Ex: 1)
血戰&血流 history 中 胡牌可能有多張牌 winnerTile 為一陣列 (Ex:[13,13,13])
好友房 winamount 會加入房主返水

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "wagerdetaildata": {
            "gamerecordid": "1585635151793-G2-183-149",
            "ecsiteid": "34863",
            "userid": "34867",
            "gameid": "18",
            "bettimestamp": "1585635331906",
            "betamount": "4",
            "validamount": "4",
            "winamount": "192",
            "result": "W",
            "balance": "1065900"
        },
        "gamerecorddata": {
            "wagerid": "0E7000000002F0HM0004",
            "gameid": "18",
            "themeid": "1",
            "roomid": "1",
            "gametype": "2",
            "mergedgamerecordid": "xxxxxx",
            "starttimestamp": "1585635151793",
            "endtimestamp": "1585635151793",
            "history": {},
        }
    },
    "Action": "CheckWagerRecordDetail"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Data": {
        "wagerdetaildata": {
            "gamerecordid": "null",
            "ecsiteid": "null",
            "userid": "null",
            "gameid": "null",
            "bettimestamp": "null",
            "betamount": "null",
            "validamount": "null",
            "winamount": "null",
            "result": "null",
            "balance": "null"
        },
        "gamerecorddata": {
            "gameid": "null",
            "themeid": "null",
            "roomid": "null",
            "gametype": "null",
            "starttimestamp": "null",
            "endtimestamp": "null",
            "history": "null",
        }
    },
    "Action": "CheckWagerRecordDetail"
}
```



# 8. LaunchGame 玩家登入遊戲

```javascript
POST {{api_server_url}}/v1
```

## Request

| 參數名稱   | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| ---------- | -------- | -------- | ---- | ------------------------------------------------------------ |
| username   | body     | string   | Y    | 使用者帳戶名稱                                               |
| gameid     | body     | string   | Y    | 遊戲id                                                       |
| amount     | body     | string   | N    | 玩遊戲金額(RMB) 目前是允許一次只玩一個遊戲，全帶玩家所有金額到一個遊戲 |
| roomNumber | body     | string   | Y    | 如果是好友房的話需要帶入房間號碼(六位數), 沒有的話帶'0'      |
| langx      | body     | string   | N    | 沒代的話是預設zh-cn                                          |
| currency   | body     | string   | N    | 沒代的話預設是所屬EC(渠道)設定的幣別(CNY, USD, SDG, PHP, THB, INR, VND, USDT) |
| category   | body     | string   | N    | 大廳類型的gameId(gameId 10000以上)指定類型的相關參數, 其他將遊戲不用帶入, enum如下面的附件區 |


**Request in body format**
```json
{
	"Request": {
		"username": "test13",
        "gameid": "18",
        "amount": "100",
        "roomNumber": "0",
        "langx": "zh-cn",
        "currency": "CNY",
        "category": "0",
	},
	"Action": "LaunchGame"
}
```



## Response

| 參數名稱      | 參數種類 | 參數型態 | 必填 | 參數說明                                              |
| ------------- | -------- | -------- | ---- | ----------------------------------------------------- |
| username      | body     | string   | Y    | 使用者帳戶名稱                                        |
| userid        | body     | string   | Y    | 使用者id                                              |
| launchgameurl | body     | string   | Y    | 登入遊戲的網址(會攜帶lang, currency, cateogry的欄位)  |
| usertoken     | body     | string   | Y    | 登入遊戲的token(會攜帶lang, currency, cateogry的欄位) |

**Response 200 OK**

```JSON
{
	"RespCode": "2000001",
	"Status": "success",
	"Action": "LaunchGame",
    "Data": {
        "launchgameurl": "xxxxxx/url/?.....",
        "username": "test13",
        "userid": "34891",
        "usertoken": "xxxxxxtoken"
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "LaunchGame",
    "Data": {
        "launchgameurl": "null",
        "username": "test13",
        "userid": "34891",
        "usertoken": "null"
    }
}
```

# 9. CheckGameReport 查詢遊戲統計

```javascript
POST {{api_server_url}}/v1
```

## Request

| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                                          |
| -------------- | -------- | -------- | ------ | ------------------------------------------------- |
| gameid         | body     | string   | option | 遊戲編號 沒有帶則查詢所有遊戲下                   |
| starttimestamp | body     | string   | Y      | 搜尋開始時間 Unix Epoch timestamp in milliseconds |
| endtimestamp   | body     | string   | Y      | 搜尋結束時間 Unix Epoch timestamp in milliseconds |

**Request in body format**
- 查詢所有遊戲下注統計

	```json
	{
		"Request":  {
			"starttimestamp": "1584356400000",
	  		"endtimestamp": "1584359999999"
	  },
		"Action": "CheckGameReport"
	}

	```

- 查詢單一遊戲下注統計

	```json
	{
		"Request":  {
			"gameid": "1",
			"starttimestamp": "1584356400000",
	        "endtimestamp": "1584359999999"
	  },
		"Action": "CheckGameReport"
	}
	```



## Response
| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                   |
| -------------- | -------- | -------- | ------ | -------------------------- |
| gameid         | body     | string   | option | 遊戲編號 或 'All' 所有遊戲 |
| betcount       | body     | string   | Y      | 查詢區間的總筆數           |
| betamount      | body     | string   | Y      | 查詢區間的總下注金額       |
| validbetamount | body     | string   | Y      | 查詢區間的總有效下注金額   |
| payoutamount   | body     | string   | Y      | 查詢區間的總贏分           |
| result         | body     | string   | Y      | 查詢區間的總淨分           |


**Response 200 OK**
```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "gameid": "All",
        "betcount": "1003",
        "betamount": "21798.58",
        "validbetamount": "39980.1966",
        "payoutamount": "251879.5643",
        "result": "39980.1966"
    },
    "Action": "CheckGameReport"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "fail",
    "Data": {
        "gameid": "null",
        "betcount": "null",
        "betamount": "null",
        "validbetamount": "null",
        "payoutamount": "null",
        "result": "null"
    },
    "Action": "CheckGameReport"
}
```



# 10. CheckPlayerReport 查詢玩家注單統計

```javascript
POST {{api_server_url}}/v1
```

## Request

| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                                                     |
| -------------- | -------- | -------- | ------ | ------------------------------------------------------------ |
| ecUserId       | body     | string   | option | ecSite會員編號 有帶 ecUserId 為查詢該會員統計; 沒帶 ecUserId 則查詢所有會員統計 |
| playerId       | body     | string   | option | 會員編號 有帶 playerId 為查詢該會員統計; 沒帶 playerId 則查詢所有會員統計 |
| starttimestamp | body     | string   | Y      | 搜尋開始時間 Unix Epoch timestamp in milliseconds            |
| endtimestamp   | body     | string   | Y      | 搜尋結束時間 Unix Epoch timestamp in milliseconds            |

**Request in body format**

- 查詢所有會員下注統計

	```json
	{
		"Request":  {
			"starttimestamp": "1584356400000",
	        "endtimestamp": "1584359999999"
	  },
		"Action": "CheckPlayerReport"
	}

	```

- 查詢單一會員下注統計

	```json
	{
		"Request":  {
			"ecUserId": "test1",
			"starttimestamp": "1584356400000",
	        "endtimestamp": "1584359999999"
	  },
		"Action": "CheckPlayerReport"
	}
    OR
	{
		"Request":  {
			"playerId": "123456",
			"starttimestamp": "1584356400000",
	        "endtimestamp": "1584359999999"
	  },
		"Action": "CheckPlayerReport"
	}
    OR
	{
		"Request":  {
            "ecUserId": "test1",
			"playerId": "123456",
			"starttimestamp": "1584356400000",
	        "endtimestamp": "1584359999999"
	  },
		"Action": "CheckPlayerReport"
	}	```

## Response
| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                         |
| -------------- | -------- | -------- | ------ | -------------------------------- |
| ecuserid       | body     | string   | option | ecsite會員名稱 或 'All' 所有會員 |
| playerId       | body     | string   | option | 會員名稱 或 'All' 所有會員       |
| betcount       | body     | string   | Y      | 查詢區間的總筆數                 |
| betamount      | body     | string   | Y      | 查詢區間的總下注金額             |
| validbetamount | body     | string   | Y      | 查詢區間的總有效下注金額         |
| payoutamount   | body     | string   | Y      | 查詢區間的總贏分                 |
| result         | body     | string   | Y      | 查詢區間的總淨分                 |

**Response 200 OK**
```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "ecuserid": "All",
        "playerId": "All",
        "betcount": "1003",
        "betamount": "291859.7609",
        "validbetamount": "291859.7609",
        "payoutamount": "251879.5643",
        "result": "39980.1966"
    },
    "Action": "CheckPlayerReport"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "ecuserid": "null",
        "playerId": "null",
        "betcount": "null",
        "betamount": "null",
        "validamount": "null",
        "payoutamount": "null",
        "result": "null"
    },
    "Action": "CheckPlayerReport"
}
```



# 11. Deposit 存款

```javascript
POST {{api_server_url}}/v1
```

## Request

| 參數名稱   | 參數種類 | 參數型態 | 必填 | 參數說明       |
| ---------- | -------- | -------- | ---- | -------------- |
| username   | body     | string   | Y    | 會員帳號       |
| amount     | body     | string   | Y    | 儲值金額       |
| payno      | body     | string   | Y    | 付款單號       |
| transferid | body     | string   | Y    | 可反查的細單id |

**Request in body format**

```json
{
    "Request": {
        "username": "test1",
        "amount": "125",
        "payno": "34863-3",
        "transferid": "xxxx-xxx-xxx",
    },
    "Action": "Deposit"
}
```



## Response

| 參數名稱  | 參數種類 | 參數型態 | 必填 | 參數說明         |
| --------- | -------- | -------- | ---- | ---------------- |
| username  | body     | string   | Y    | 會員帳號         |
| txid      | body     | string   | Y    | 轉帳編號         |
| payway    | body     | string   | Y    | 存款"1",提款"-1" |
| currency  | body     | string   | Y    | 貨幣單位         |
| reqamount | body     | string   | Y    | 存款金額         |
| balance   | body     | string   | Y    | 存款後餘額       |
| payno     | body     | string   | Y    | 付款單號         |

**Response 200 OK**
```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "moneydata": {
            "username": "test1",
            "txid": "1083048",
            "payway": "1",
            "currency": "RMB",
            "reqamount": "125",
            "balance": "1000",
            "payno": "34863-3"

        }
    },
    "Action": "Deposit"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "moneydata": {
            "username": "null",
            "txid": "null",
            "payway": "null",
            "currency": "null",
            "reqamount": "null",
            "balance": "null",
            "payno": "null"
        }
    },
    "Action": "Deposit"
}
```

# 12. Withdraw 取款

```javascript
POST {{api_server_url}}/v1
```

## Request

| 參數名稱   | 參數種類 | 參數型態 | 必填 | 參數說明       |
| ---------- | -------- | -------- | ---- | -------------- |
| username   | body     | string   | Y    | 會員帳戶       |
| amount     | body     | string   | Y    | 儲值金額       |
| payno      | body     | string   | Y    | 付款單號       |
| transferid | body     | string   | Y    | 可反查的細單id |

**Request in body format**

```json
{
    "Request": {
        "username": "test1",
        "amount": "125",
        "payno": "34863-3",
        "transferid": "xxxx-xxx-xxx",
    },
    "Action": "Withdraw"
}
```



## Response

| 參數名稱  | 參數種類 | 參數型態 | 必填 | 參數說明         |
| --------- | -------- | -------- | ---- | ---------------- |
| username  | body     | string   | Y    | 會員帳號         |
| txid      | body     | string   | Y    | 轉帳編號         |
| payway    | body     | string   | Y    | 存款"1",提款"-1" |
| currency  | body     | string   | Y    | 貨幣單位         |
| reqamount | body     | string   | Y    | 取款金額         |
| balance   | body     | string   | Y    | 取款後餘額       |
| payno     | body     | string   | Y    | 付款單號         |



**Response 200 OK**
```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "moneydata": {
            "username": "test1",
            "txid": "1083056",
            "payway": "-1",
            "currency": "RMB",
            "reqamount": "1",
            "balance": "999",
            "payno": "34863-3"
        }
    },
    "Action": "Withdraw"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "moneydata": {
            "username": "null",
            "txid": "null",
            "payway": "null",
            "currency": "null",
            "reqamount": "null",
            "balance": "null",
            "payno": "null"
        }
    },
    "Action": "Withdraw"
}
```



# 13. KickOutMem 踢玩家

```javascript
POST {{api_server_url}}/v1
```

## Request

| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明       |
| -------- | -------- | -------- | ---- | -------------- |
| username | body     | string   | Y    | 使用者帳戶名稱 |

**Request in body format**

```json
{
	"Request": {
		"username": "testMember01",
	},
	"Action": "KickOutMem"
}
```



## Response

| 參數名稱  | 參數種類 | 參數型態 | 必填 | 參數說明         |
| --------- | -------- | -------- | ---- | ---------------- |
| username  | body     | string   | Y    | 會員帳號         |
| txid      | body     | string   | Y    | 轉帳編號         |
| payway    | body     | string   | Y    | 存款"1",提款"-1" |
| currency  | body     | string   | Y    | 貨幣單位         |
| reqamount | body     | string   | Y    | 取款金額         |
| balance   | body     | string   | Y    | 取款後餘額       |


**Response 200 OK**
```json
{
	"RespCode": "2000001",
	"Status": "success",
	"Action": "KickOutMem",
    "Data": {
        "username": "testMember01",
        "userid": "1234567"
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "KickOutMem",
    "Data": {
        "username": "testMember01",
        "userid": "1234567"
    }
}
```



# 14. GetTransactionsRecordDetail 獲取單筆轉帳紀錄

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱   | 參數種類 | 參數型態 | 必填 | 參數說明 |
| ---------- | -------- | -------- | ---- | -------- |
| transpayno | body     | string   | Y    | 轉帳單號 |

**Request in body format**

- 查詢時間區段內的所有轉帳記錄

```json
{
	"Request":  {
			"transpayno": "34863-8"
  },
	"Action": "GetTransactionsRecordDetail"
}
```

## Response

| 參數名稱    | 參數種類 | 參數型態 | 必填 | 參數說明            |
| ----------- | -------- | -------- | ---- | ------------------- |
| transuserid | body     | string   | Y    | 會員轉帳唯一序號    |
| transrecid  | body     | string   | Y    | 交易回饋的紀錄 id   |
| transpayno  | body     | string   | Y    | 存提交易時定義的 id |
| transamount | body     | string   | Y    | 交易金額            |
| transdate   | body     | string   | Y    | 交易時間            |
| transpayway | body     | string   | Y    | 存款"1",提款"-1"    |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "transdata": [
            {
                "transuserid": "34867",
                "transrecid": "1043279",
                "transpayno": "34863-3",
                "transamount": "125",
                "transdate": "1586423614639",
                "transpayway": "1"
            },
        ]
    },
    "Action": "GetTransactionsRecordDetail"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Data": {
        "transdata": {
            "transuserid": "null",
            "transrecid": "null",
            "transpayno": "null",
            "transamount": "null",
            "transdate": "null",
            "transpayway": "null"
        },
    },
    "Action": "GetTransactionsRecordDetail"
}
```



# 15. CreateEcSite 創建代理

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱        | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| --------------- | -------- | -------- | ---- | ------------------------------------------------------------ |
| name            | body     | string   | Y    | 代理商名稱                                                   |
| password        | body     | string   | Y    | 代理商密碼                                                   |
| authorizedGames | body     | object   | Y    | 授權遊戲<br />key 為 gamId<br />acitve: 營運與否<br />defaultSync: 同步原本遊戲風控設定與否 |
| blocked         | body     | boolean  | Y    | 封鎖                                                         |
| autoTransfer    | body     | boolean  | Y    | 自動轉帳                                                     |
| hookUrl         | body     | string   | Y    | 自動轉帳網址                                                 |

**Request in body format**

```json
{
	"Request":  {
    "name": "testhelloworldhhhh",
		"password": "123456",
		"authorizedGames": {
			"1": {
				"active": true,
				"defaultSync": true
			}
		},
		"blocked": false,
		"autoTransfer": false,
		"hookUrl": "https://google.com"
  },
	"Action": "CreateEcSite"
}
```

## Response

| 參數名稱        | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| --------------- | -------- | -------- | ---- | ------------------------------------------------------------ |
| name            | body     | string   | Y    | 代理商名稱                                                   |
| password        | body     | string   | Y    | 代理商密碼                                                   |
| authorizedGames | body     | object   | Y    | 授權遊戲<br />key 為 gamId<br />acitve: 營運與否<br />defaultSync: 同步原本遊戲風控設定與否 |
| blocked         | body     | boolean  | Y    | 封鎖                                                         |
| autoTransfer    | body     | boolean  | Y    | 自動轉帳                                                     |
| hookUrl         | body     | string   | Y    | 自動轉帳網址                                                 |
| registerTime    | body     | number   | Y    | 註冊時間                                                     |
| id              | body     | string   | Y    | 代理商編號                                                   |



**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "ecsitedata": {
            "name": "testhelloworldhhhh",
            "password": "$2b$11$dXKnFTuiytTVXaU8HBsoHe9UjLbNAtWck62oxz1qntPdnXthsblfG",
            "authorizedGames": {
                "1": {
                    "active": true,
                    "defaultSync": true
                }
            },
            "blocked": false,
            "autoTransfer": false,
            "hookUrl": "https://google.com",
            "registerTime": 1589167080666,
            "id": "51685991"
        }
    },
    "Action": "CreateEcSite"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Data": {
        "ecsitedata": {
            "name": "null",
            "password": "null",
            "authorizedGames": "null",
            "blocked": "null",
            "autoTransfer": "null",
            "hookUrl": "null",
            "registerTime": "null",
            "id": "null"
        }
    },
    "Action": "CreateEcSite"
} 
```



# 16. GetAuthGameForEcSite 獲取風控設定

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| gameid   | body     | string   | Y    | 遊戲編號 |

**Request in body format**

```json
{
	"Request": {
		"gameid": "1"
	},
	"Action": "GetAuthGameForEcSite"
}
```

## Response

| 參數名稱        | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| --------------- | -------- | -------- | ---- | ------------------------------------------------------------ |
| name            | body     | string   | Y    | 代理商名稱                                                   |
| password        | body     | string   | Y    | 代理商密碼                                                   |
| authorizedGames | body     | object   | Y    | 授權遊戲<br />key 為 gamId<br />acitve: 營運與否<br />defaultSync: 同步原本遊戲風控設定與否 |
| blocked         | body     | boolean  | Y    | 封鎖                                                         |
| autoTransfer    | body     | boolean  | Y    | 自動轉帳                                                     |
| hookUrl         | body     | string   | Y    | 自動轉帳網址                                                 |
| registerTime    | body     | number   | Y    | 註冊時間                                                     |
| id              | body     | string   | Y    | 代理商編號                                                   |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "id": "1",
        "name": {
            "en-us": "NuNu",
            "zh-cn": "搶庄牛牛",
            "zh-tw": "搶庄牛牛"
        },
        "active": true,
        "defaultSync": false,
        "settingMap": [
            {
                "themeId": "1",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 20000000,
                "ante": 1,
                "juiceRatio": 0.05,
                "minCoin": 15,
                "maxCoin": 20000000
            },
            {
                "themeId": "2",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 20000000,
                "ante": 5,
                "juiceRatio": 0.05,
                "minCoin": 75,
                "maxCoin": 20000000
            },
            {
                "themeId": "3",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 20000000,
                "ante": 10,
                "juiceRatio": 0.05,
                "minCoin": 150,
                "maxCoin": 20000000
            },
            {
                "themeId": "4",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 20000000,
                "ante": 30,
                "juiceRatio": 0.05,
                "minCoin": 450,
                "maxCoin": 20000000
            }
        ]
    },
    "Action": "GetAuthGameForEcSite"
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Data": {
        "id": "null",
        "name": {
            "en-us": "null",
            "zh-cn": "null",
            "zh-tw": "null",
        },
        "active": "null",
        "defaultSync": "null",
        "settingMap": "null"
    },
    "Action": "GetAuthGameForEcSite"
}
```



# 17. UpdateAuthGameForEcSite 代理更新風控

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| gameid   | body     | string   | Y    | 遊戲編號 |

**Request in body format**

```json
{
	"Request": {
		"gameid": "1",
		"active": true,
		"defaultSync": false,
		"settingMap": [
            {
                "themeId": "1",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 2000000,
                "ante": 100,
                "juiceRatio": 1,
                "minCoin": 2,
                "maxCoin": 1000
            },
            {
                "themeId": "2",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 40000000,
                "ante": 200,
                "juiceRatio": 1,
                "minCoin": 2,
                "maxCoin": 1000
            },
            {
                "themeId": "3",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 100,
                "ante": 500,
                "juiceRatio": 1,
                "minCoin": 2,
                "maxCoin": 1000
            },
            {
                "themeId": "4",
                "basisRtp": 0.975,
                "expectRtp": 0.9,
                "rtpTorelance": 0.001,
                "posMagnification": 1,
                "negMagnification": 1,
                "maxCompensation": 100,
                "ante": 1000,
                "juiceRatio": 1,
                "minCoin": 2,
                "maxCoin": 1000
            }
		]
	},
	"Action": "UpdateAuthGameForEcSite"
}
```

## Response

| 參數名稱        | 參數種類 | 參數型態 | 必填 | 參數說明                                                     |
| --------------- | -------- | -------- | ---- | ------------------------------------------------------------ |
| name            | body     | string   | Y    | 代理商名稱                                                   |
| password        | body     | string   | Y    | 代理商密碼                                                   |
| authorizedGames | body     | object   | Y    | 授權遊戲<br />key 為 gamId<br />acitve: 營運與否<br />defaultSync: 同步原本遊戲風控設定與否 |
| blocked         | body     | boolean  | Y    | 封鎖                                                         |
| autoTransfer    | body     | boolean  | Y    | 自動轉帳                                                     |
| hookUrl         | body     | string   | Y    | 自動轉帳網址                                                 |
| registerTime    | body     | number   | Y    | 註冊時間                                                     |
| id              | body     | string   | Y    | 代理商編號                                                   |

**Response 200 OK**

```JSON
{
    "RespCode": "2040001",
    "Status": "success",
    "Action": "UpdateAuthGameForEcSite",
    "Data": {}
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
    "RespCode": "5000001",
    "Status": "success",
    "Action": "UpdateAuthGameForEcSite",
    "Data": {}
}
```

# 18. GetGameVersion 獲取遊戲版本號

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| gameid   | body     | string   | Y    | 遊戲編號 |

**Request in body format**

- 查詢遊戲

```json
{
    "Request":  {
        "gameid": "1"
    },
    "Action": "GetGameVersion"
}
```

## Response

| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| version  | body     | string   | Y    | 遊戲版號 |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "version": "1.0.0",
    },
    "Action": "GetGameVersion"
}
```

# 19. GetGameDownloadLocation 獲取遊戲下載位置

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱 | 參數種類 | 參數型態 | 必填 | 參數說明 |
| -------- | -------- | -------- | ---- | -------- |
| gameid   | body     | string   | Y    | 遊戲編號 |

**Request in body format**

- 查詢遊戲

```json
{
    "Request":  {
        "gameid": "1"
    },
    "Action": "GetGameDownloadLocation"
}
```

## Response

| 參數名稱         | 參數種類 | 參數型態 | 必填 | 參數說明     |
| ---------------- | -------- | -------- | ---- | ------------ |
| downloadLocation | body     | string   | Y    | 遊戲下載位置 |

**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "downloadLocation": "http://squirrel-dev.paradise-soft.com.tw/p/demeter/cocos/g2/web-mobile.zip",
    },
    "Action": "GetGameDownloadLocation"
}
```

# 20. CheckClubRecords 查詢俱樂部領取返水紀錄

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                                                     |
| -------------- | -------- | -------- | ------ | ------------------------------------------------------------ |
| username       | body     | string   | option | 會員名稱 帶 username 為取該會員領取返水紀錄，不帶則取得所有會員注單列表 |
| userid         | body     | string   | option | 會員編號 帶 userid 為取該會員領取返水紀錄，不帶則取得所有會員注單列表 |
| starttimestamp | body     | string   | Y      | 搜尋開始時間                                                 |
| endtimestamp   | body     | string   | Y      | 搜尋結束時間                                                 |
| page           | body     | string   | Y      | 頁數(最小1)                                                  |
| limit          | body     | string   | option | 每頁筆數(預設20,最小20)                                      |


**Request in body format**

- 查詢單一會員俱樂部領取返水紀錄

```json
{
	"Request":  {
                    "username": "test1",
                    "starttimestamp": "1633046400000",
                    "endtimestamp": "1634255999000",
                    "page": "1",
                    "limit": "20"
                },
	"Action": "CheckClubRecords"
}
```

## Response

| 參數名稱        | 參數種類 | 參數型態 | 必填 | 參數說明           |
| --------------- | -------- | -------- | ---- | ------------------ |
| totalpage       | body     | string   | Y    | 總頁數             |
| page            | body     | string   | Y    | 第幾頁             |
| clubrecordsdata | body     | object   | Y    | 俱樂部領取返水紀錄 |

**clubrecordsdata 內的參數說明**

| 參數名稱                   | 參數種類 | 參數型態 | 必填 | 參數說明                                        |
| -------------------------- | -------- | -------- | ---- | ----------------------------------------------- |
| _key                       | body     | string   | Y    | 領取紀錄編號                                    |
| clubId                     | body     | string   | Y    | 俱樂部編號                                      |
| clubName                   | body     | string   | Y    | 俱樂部名稱                                      |
| ecName                     | body     | string   | Y    | 渠道名稱                                        |
| ecSiteId                   | body     | string   | Y    | 渠道Id                                          |
| permissionName             | body     | string   | Y    | 權限等級                                        |
| playerId                   | body     | string   | Y    | 玩家Id                                          |
| playerName                 | body     | string   | Y    | 玩家名稱                                        |
| playerTotalClubJuiceAmount | body     | number   | Y    | 累積返水(截至當前總俱樂部抽水(個人))            |
| realizedClubJuiceAmount    | body     | number   | Y    | 累積已領取返水 (截至當前已實現俱樂部抽水(個人)) |
| timestamp                  | body     | number   | Y    | 操作時間                                        |
| unrealizedClubJuiceAmount  | body     | number   | Y    | 未領取的返水(截至當前未實現俱樂部抽水(個人))    |
| withdrawAmount             | body     | string   | Y    | 此次領取返水金額                                |


**Response 200 OK**

```JSON
{
    "RespCode": "2000001",
    "Status": "success",
    "Data": {
        "totalpage": "1",
        "page": "1",
        "clubrecordsdata": [
            {
                "_key": "23731202",
                "playerId": "35671460",
                "clubId": "23729669",
                "withdrawAmount": 1,
                "realizedClubJuiceAmount": 1,
                "unrealizedClubJuiceAmount": 89,
                "timestamp": 1633403926021,
                "clubName": "測試一下",
                "playerTotalClubJuiceAmount": 90,
                "permissionName": "admin",
                "ecSiteId": "1",
                "ecName": "testEcSite",
                "playerName": "jjj001"
            },
            {
                "_key": "23733230",
                "playerId": "35671460",
                "clubId": "23729669",
                "withdrawAmount": 1,
                "realizedClubJuiceAmount": 2,
                "unrealizedClubJuiceAmount": 88,
                "timestamp": 1633404818178,
                "clubName": "測試一下",
                "playerTotalClubJuiceAmount": 90,
                "permissionName": "admin",
                "ecSiteId": "1",
                "ecName": "testEcSite",
                "playerName": "jjj001"
            },
        ]
    },
    "Action": "CheckClubRecords"
}
```

**Response 4000001 搜尋參數錯誤**

```json
{
    "Status": "fail",
    "RespCode": "4000001",
    "Data": {
        "clubrecordsdata": [],
        "totalpage": "1",
        "page": "1"
    },
    "Action": "CheckClubRecords"
}
```
**Response 2040404 查無玩家**

```json
{
    "Status": "fail",
    "RespCode": "2040404",
    "Data": {
        "clubrecordsdata": [],
        "totalpage": "1",
        "page": "1"
    },
    "Action": "CheckClubRecords"
}
```

# 21. LaunchLobby 啟動遊戲大廳

```javascript
POST {{api_server_url}}/v1
```

## Request


| 參數名稱       | 參數種類 | 參數型態 | 必填   | 參數說明                                                     |
| -------------- | -------- | -------- | ------ | ------------------------------------------------------------ |
| username       | body     | string   | Y | 會員名稱 帶 username 為取該會員領取返水紀錄，必帶 |
| userid         | body     | string   | option | 會員編號 帶 userid 為取該會員領取返水紀錄，可以不帶 |
| langx | body     | string   | Y      | 語系，zh-cn: 簡體、en-us: 英文、zh-tw: 繁體、ja-jp: 日文、th-th: 泰文、ko-kr: 韓文、vi-vn: 越南文，預設 zh-tw: 繁體 |


**Request in body format**

- 查詢單一會員俱樂部領取返水紀錄

```json
{
	"Request":  {
                    "username": "test1",
                    "langx": "zh-tw"
                },
	"Action": "LaunchLobby"
}
```

## Response

| 參數名稱      | 參數種類 | 參數型態 | 必填 | 參數說明                                              |
| ------------- | -------- | -------- | ---- | ----------------------------------------------------- |
| launchlobbyurl | body     | string   | Y    | 登入遊戲的網址(會攜帶lang, currency, cateogry的欄位)  |

**Response 200 OK**

```JSON
{
	"RespCode": "2000001",
	"Status": "success",
	"Action": "LaunchLobby",
    "Data": {
        "launchlobbyurl": "xxxxxx/url/?....."
    }
}
```

**Response 500 Internal Server Error 內部系統錯誤**

```json
{
	"RespCode": "5000001",
	"Status": "fail",
	"Action": "LaunchLobby",
    "Data": {
        "launchgameurl": "null"
    }
}
```


# 附件

## Response & Error code 對應表

| Code    | 錯誤訊息                         |
| ------- | -------------------------------- |
| 2000001 | 成功，有回傳資料                 |
| 2040001 | 成功，不需要回傳資料(no content) |
| 2040404 | 成功，無資料                     |
| 4030001 | 錯誤，代理權限驗證錯誤           |
| 4000002 | 錯誤，參數簽名驗證錯誤           |
| 4030003 | 錯誤，代理幣別驗證錯誤           |
| 4000001 | 錯誤，參數相關驗證錯誤           |
| 4040099 | 錯誤，會員資料重複               |
| 4040121 | 錯誤，無代理資料                 |
| 4040001 | 錯誤，無相關資料                 |
| 4030104 | 錯誤，金額不足                   |
| 4030105 | 錯誤，存款超過限制               |
| 4030106 | 錯誤，提款餘額不足               |
| 4030102 | 錯誤，重複登陸                   |
| 5000001 | 錯誤，系統執行錯誤               |
| 5000002 | 錯誤，系統維護中                 |


## History 說明


### Slot 圖標對應表

| Code | Symbol  |
| ---- | ------- |
| 50   | WILD    |
| 100  | FWILD   |
| 20   | SCATTER |
| 1    | H1      |
| 2    | H2      |
| 3    | H3      |
| 4    | H4      |
| 5    | H5      |
| 6    | H6      |
| 7    | H7      |
| 8    | H8      |
| 9    | H9      |
| 11   | N1      |
| 12   | N2      |
| 13   | N3      |
| 14   | N4      |
| 15   | N5      |
| 16   | N6      |
| 17   | N7      |
| 18   | N8      |
| 19   | N9      |

### Slot 消除類 (深海,埃及,瑪雅) 

#### 陣列裡面為該次的遊戲次數,如果有免費遊戲,該陣列裡面會有對多筆紀錄

history 裡的物件結構
| 參數名稱      | 參數型態 | 參數說明                             |
| ------------- | -------- | ------------------------------------ |
| AllReels      | array    | 盤面結果                             |
| RandomNumList | array    | 轉輪表位置index                      |
| LongWildIndex | integer  | 滿軸位置 如果是5代表本次旋轉沒有滿軸 |
| TotalWinBonus | float    | 總派彩金額                           |
| EliminateList | array    | 消除結果清單                         |

EliminateList 裡物件的結構
| 參數名稱    | 參數型態 | 參數說明         |
| ----------- | -------- | ---------------- |
| WinPos      | array    | 消除位置對應表   |
| IsEliminate | boolean  | 是否為消除       |
| NextReels   | array    | 下次旋轉盤面結果 |
| WinBonus    | float    | 該次旋轉派彩金額 |
| WinLineList | array    | 消除圖標結果清單 |

WinLineList 裡物件的結構
| 參數名稱        | 參數型態 | 參數說明         |
| --------------- | -------- | ---------------- |
| SpecialMultiple | integer  | 倍率             |
| X               | integer  | 幾條線           |
| WinNum          | integer  | 圖標             |
| WinBonus        | float    | 該次消除派彩金額 |
| LineNum         | integer  | 幾連線           |


**history response example**

```json
[
    {
        "AllReels": [
            [
                14,
                11,
                13
            ],
            [
                14,
                3,
                13
            ],
            [
                3,
                13,
                4
            ],
            [
                4,
                12,
                20
            ],
            [
                5,
                12,
                3
            ]
        ],
        "RandomNumList": [
            47,
            43,
            67,
            40,
            32
        ],
        "LongWildIndex": 5,
        "TotalWinBonus": 0.42,
        "EliminateList": [
            {
                "WinPos": [
                    [
                        0,
                        0,
                        1
                    ],
                    [
                        0,
                        0,
                        1
                    ],
                    [
                        0,
                        1,
                        0
                    ],
                    [
                        0,
                        0,
                        0
                    ],
                    [
                        0,
                        0,
                        0
                    ]
                ],
                "IsEliminate": true,
                "NextReels": [
                    [
                        5,
                        14,
                        11
                    ],
                    [
                        4,
                        14,
                        3
                    ],
                    [
                        14,
                        3,
                        4
                    ],
                    [
                        4,
                        12,
                        20
                    ],
                    [
                        5,
                        12,
                        3
                    ]
                ],
                "WinBonus": 0.3,
                "WinLineList": [
                    {
                        "SpecialMultiple": 1,
                        "X": 1,
                        "WinNum": 13,
                        "WinBonus": 0.3,
                        "LineNum": 3
                    }
                ]
            },
            {
                "WinPos": [
                    [
                        0,
                        1,
                        0
                    ],
                    [
                        0,
                        1,
                        0
                    ],
                    [
                        1,
                        0,
                        0
                    ],
                    [
                        0,
                        0,
                        0
                    ],
                    [
                        0,
                        0,
                        0
                    ]
                ],
                "IsEliminate": true,
                "NextReels": [
                    [
                        1,
                        5,
                        11
                    ],
                    [
                        11,
                        4,
                        3
                    ],
                    [
                        2,
                        3,
                        4
                    ],
                    [
                        4,
                        12,
                        20
                    ],
                    [
                        5,
                        12,
                        3
                    ]
                ],
                "WinBonus": 0.12,
                "WinLineList": [
                    {
                        "SpecialMultiple": 1,
                        "X": 1,
                        "WinNum": 14,
                        "WinBonus": 0.12,
                        "LineNum": 3
                    }
                ]
            }
        ]
    }
]
```


### Slot 連線類 (電音,封神) 

#### resultList如果有免費遊戲,該陣列裡面會有對多筆紀錄

history 裡的物件結構
| 參數名稱   | 參數型態 | 參數說明                           |
| ---------- | -------- | ---------------------------------- |
| totalBonus | float    | 總派彩金額                         |
| totalBet   | float    | 投注額                             |
| resultList | array    | 遊戲結果清單, 有free game 會有多筆 |
| allReels   | array    | 對應resultList,紀錄遊戲盤面        |

resultList 裡物件的結構
| 參數名稱      | 參數型態 | 參數說明             |
| ------------- | -------- | -------------------- |
| anyNumPos     | string   | 百搭位置  Ex:"00000" |
| randomNumList | boolean  | 轉輪表位置index      |
| totalWinBonus | array    | 該次旋轉派彩金額     |
| WinLineList   | array    | 消除圖標結果清單     |

WinLineList 裡物件的結構
| 參數名稱        | 參數型態 | 參數說明 |
| --------------- | -------- | -------- |
| lineIndex       | integer  | 連線位置 |
| winNum          | integer  | 圖標     |
| winBonus        | float    | 派彩     |
| lineNum         | integer  | 幾連線   |
| specialMultiple | integer  | 倍率     |

**history response example**

```json
{
    "totalBonus": 0,
    "totalBet": 1.8,
    "resultList": [
        {
            "anyNumPos": "00000",
            "randomNumList": [
                1,
                1,
                1,
                1,
                1
            ],
            "totalWinBonus": 0,
            "winLineList": [
                {
                    "lineIndex": 5,
                    "winNum": 3,
                    "winBonus": 1.8,
                    "lineNum": 3,
                    "specialMultiple": 1
                }
            ]
        }
    ],
    "allReels": [
        [
            [
                1,
                2,
                3
            ],
            [
                11,
                2,
                14
            ],
            [
                1,
                14,
                13
            ],
            [
                2,
                14,
                13
            ],
            [
                1,
                12,
                13
            ]
        ]
    ]
}
```

### 森林舞會 轉盤開獎結果對應表
| 結果 | 獎項         |
| ---- | ------------ |
| 0    | 獅子(黃燈)   |
| 1    | 獅子(綠燈)   |
| 2    | 獅子(紅燈)   |
| 3    | 熊貓(黃燈)   |
| 4    | 熊貓(綠燈)   |
| 5    | 熊貓(紅燈)   |
| 6    | 猴子(黃燈)   |
| 7    | 猴子(綠燈)   |
| 8    | 猴子(紅燈)   |
| 9    | 兔子(黃燈)   |
| 10   | 兔子(綠燈)   |
| 11   | 兔子(紅燈)   |
| 12   | 大三元(獅子) |
| 13   | 大三元(熊貓) |
| 14   | 大三元(猴子) |
| 15   | 大三元(兔子) |
| 16   | 大四喜(黃燈) |
| 17   | 大四喜(綠燈) |
| 18   | 大四喜(紅燈) |

### 森林舞會 

history 裡的物件結構
| 參數名稱             | 參數型態 | 參數說明                                                     |
| -------------------- | -------- | ------------------------------------------------------------ |
| WheelDraw            | integer  | 轉盤開獎結果                                                 |
| SceneDraw            | integer  | 場景開獎結果：0.莊 1.閒 2.和                                 |
| Odd                  | array    | 賠倍列表，數字代表 該陣列的index對應到 WheelDraw 開獎結果對應表 <br> ex第一個值為40,其index為0, 所以獅子(黃燈)賠率為40倍 |
| PlayerBetHistoryList | array    | 該局所有玩家的下注結果                                       |


PlayerBetHistoryList 裡物件的結構
#### 該局有幾個玩家下注,該陣列就會有幾個物件
| 參數名稱     | 參數型態 | 參數說明                                                     |
| ------------ | -------- | ------------------------------------------------------------ |
| PlayerId     | string   | PlayerId ex:"16957519"                                       |
| WheelIncome  | integer  | 輪盤總贏分                                                   |
| WheelOutcome | integer  | 輪盤總下注                                                   |
| SceneIncome  | integer  | 場景總贏分                                                   |
| SceneOutcome | integer  | 場景總下注                                                   |
| BetList      | array    | 玩家個人下注紀錄，數字代表 index：0.獅子(黃燈) 1.獅子(綠燈) 2.獅子(紅燈) 3.熊貓(黃燈) 4.熊貓(綠燈) 5.熊貓(紅燈) 6.猴子(黃燈) 7.猴子(綠燈) 8.猴子(紅燈) 9.兔子(黃燈) 10.兔子(綠燈) 11.兔子(紅燈) 12.莊 13.閒 14.和" |
| WinList      | array    | 玩家個人贏分紀錄，數字代表 index：0.獅子(黃燈) 1.獅子(綠燈) 2.獅子(紅燈) 3.熊貓(黃燈) 4.熊貓(綠燈) 5.熊貓(紅燈) 6.猴子(黃燈) 7.猴子(綠燈) 8.猴子(紅燈) 9.兔子(黃燈) 10.兔子(綠燈) 11.兔子(紅燈) 12.莊 13.閒 14.和 |

**history response example**

```json
{
    "WheelDraw": 11,
    "SceneDraw": 0,
    "Odd": [
        40,
        31,
        28,
        20,
        17,
        14,
        11,
        10,
        8,
        8,
        6,
        5,
        2,
        2,
        8
    ],
    "PlayerBetHistoryList": [
        {
            "PlayerId": "15829317",
            "WheelIncome": 5,
            "WheelOutcome": 12,
            "SceneIncome": 0,
            "SceneOutcome": 0,
            "BetList": [
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                0,
                0,
                0
            ],
            "WinList": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                5,
                0,
                0,
                0
            ]
        },
        {
            "PlayerId": "16957519",
            "WheelIncome": 50,
            "WheelOutcome": 120,
            "SceneIncome": 20,
            "SceneOutcome": 30,
            "BetList": [
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10
            ],
            "WinList": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                50,
                20,
                0,
                0
            ]
        }
    ]
}
```

### 富貴捕魚 魚隻結果對應表
| 結果 |    魚種    |
| :--: | :--------: |
| 101  |    飛魚    |
| 102  |   小丑魚   |
| 103  |    關刀    |
| 104  |   藍倒吊   |
| 105  |   比目魚   |
| 106  |   獅子魚   |
| 107  |    龍蝦    |
| 108  |    河豚    |
| 109  |    海馬    |
| 110  |    海龜    |
| 111  |   燈籠魚   |
| 112  |    企鵝    |
| 201  |   魔鬼魚   |
| 202  |    鱷魚    |
| 203  |   美人魚   |
| 204  |    河豚    |
| 205  |    蟾蜍    |
| 206  |  巨型蟾蜍  |
| 207  |    虎鯨    |
| 301  |   八爪魚   |
| 302  |   玄武龜   |
| 303  |    青龍    |
| 401  | 八爪魚Boss |
| 402  | 玄武龜Boss |
| 403  |  青龍Boss  |
| 501  |  瘋狂鑽頭  |
| 502  |  粒子激光  |
| 503  |  超級炸彈  |
| 504  |  神密黑洞  |
| 505  |  雷神之力  |
| 506  |  幸運轉盤  |
| 666  |  Jackpot   |

### 富貴捕魚 子彈類對應表
| 結果 |      子彈      |
| :--: | :------------: |
|  0   |    一般子弹    |
| 401  | 八爪鱼Boss大招 |
| 402  | 玄武龟Boss大招 |
| 403  |  青龙Boss大招  |
| 501  |    疯狂钻头    |
| 502  |    粒子激光    |
| 503  |    超级炸弹    |
| 504  |    神密黑洞    |
| 505  |    雷神之力    |
| 506  |    幸运转盘    |



### 富貴捕魚 

history 的物件結構(為子彈資料組成的陣列)

#### 子彈資料結構,每一筆遊戲記錄最多只會紀錄100發子彈資訊
| 參數名稱   | 參數型態 | 參數說明                     |
| ---------- | :------- | :--------------------------- |
| ante       | integer  | 子彈金額                     |
| skillType  | integer  | 技能類型(參考子彈類型對應表) |
| deadFishes | array    | 死魚(這發子彈打死了多少魚種) |

#### deadFishes 資料結構


| 參數名稱       | 參數型態 | 參數說明                     |
| :------------- | :------- | :--------------------------- |
| fishId         | string   | 魚隻流水編號                 |
| fishType       | integer  | 魚隻類型(參考魚隻結果對應表) |
| position       | array    | 魚隻死掉位置                 |
| luckyMultiples | array    | 幸運轉輪的倍率               |
| luckyReelIdxs  | array    | 幸運轉輪的index              |
| odds           | integer  | 魚隻賠率                     |
| pay            | integer  | 賠付金額                     |
| skillId        | string   | 技能流水編號                 |
| skillType      | integer  | 技能類型(參考子彈類型對應表) |


**history response example**

```json
[
    {
        "ante": 1000,
        "skillType": 0,
        "deadFishes": [
            {
                "fishId": "f107-c99sh7d1npb7c2c4o9bg",
                "fishType": 107,
                "position": [
                    -273.0483613952996,
                    -113.81720881200772
                ],
                "odds": 6,
                "pay": 6000,
                "skillType": 0,
                "skillId": "",
                "luckyReelIdxs": [],
                "luckyMultiples": []
            }
        ]
    },
    {
        "fishId": "f506-c99tpbl1npb7c2c88ck0",
        "fishType": 506,
        "position": [
            -166.87116902023263,
            0.30067639723165485
        ],
        "odds": 280,
        "pay": 280000,
        "skillType": 506,
        "skillId": "s506-c99tphd1npb7c2c8925g",
        "luckyReelIdxs": [
            18,
            22,
            31
        ],
        "luckyMultiples": [
            14,
            20,
            1
        ]
    }
    .
    .
    .
    .
]
```


# 單一錢包接口文檔

單一錢包 API `不需要`在 POST 的 Body 在傳輸的時候會給代理商 JWT 的 secret 作為資料傳輸的加密金鑰， 將要傳 JSON 的 Body 加密後傳輸

## 文件目的

- 本文檔介紹平台接入遊戲系統流程、接口參數規範、加密驗簽規則，並於文末提供各語言版本串接範例。

## 傳輸規範

- 使用TCP/IP作為傳輸層協議
- 使用HTTP作為應用層協議
- 傳遞參數格式皆為json(須使用Content-type規範header)

## 安全規範

- 所有接口必須校驗 MD5 sign 簽名值正確無誤,再處理業務邏輯
- 建議平台將遊戲系統通知主機加入信任名單,防範其餘外來主機偽裝調用接口

### MD5簽名規範

傳送的 body 的部分內容為

```json
{
"PlayerId": "12345",
"Xid": "sdf32-sdf23r-sf32r",
"GameId": "15",
"BetAmount": 100000(遊戲幣是使用 1:10000),
"WinAmount": 300000(遊戲幣是使用 1:10000),
"StartTimestamp": 123141245214,
"Sign":"58df2b36d1edb86c37f6f5cb363587b0"
}
```

`依序取出 Xid + PlayerId + GameId + BetAmount(轉成 string) + WinAmount(轉成 string) + StartTimestamp(轉成 string)`

以上範例為

`"12345" + "sdf32-sdf23r-sf32r" + "15" + "100000" + "300000" + "123141245214"`

變成完整的 string 串接在一起 `12345sdf32-sdf23r-sf32r15100000300000123141245214`

再經過 Md5 之後會輸出 `58df2b36d1edb86c37f6f5cb363587b0`

效驗過才算是有效的單，效驗不過，請不要執行!!!



## 單一錢包接口說明平台方藥提供的 API 接口

### 玩家當前餘額

玩家在平台的當前餘額，不需要 MD5 效驗。

### Reuqest

- Method: POST
- URL: `平台提供自定義接口位置`
- Headers： Content-Type:application/json
- Body:

```json
{
"playerId": "12345",
"nickname": "player1",
"username": "player1",
}
```

| 欄位     | 屬性   | 說明            |
| -------- | ------ | --------------- |
| playerId | string | 遊戲平台玩家 Id |
| nickname | string | 玩家的暱稱      |
| username | string | 玩家的暱稱      |

#### Response

- Body:
```json
{
"resCode": "0",
"data": {
      "balance": 50000,
    }
}
```

| 欄位    | 屬性   | 說明                                               |
| ------- | ------ | -------------------------------------------------- |
| resCode | string | 0: 成功、1: 失敗                                   |
| balance | number | 玩家當前在平台上的餘額(需要乘上 10000，再傳送過來) |





### 老虎機專用 SlotSpin

專門老虎機專用的接口

#### Reuqest

- Method: POST
- URL: `平台提供自定義接口位置`
- Headers： Content-Type:application/json
- Body:

```json
{
"PlayerId": "12345",
"Xid": "sdf32-sdf23r-sf32r",
"GameId": "15",
"BetAmount": 100000(遊戲幣是使用 1:10000),
"WinAmount": 300000(遊戲幣是使用 1:10000),
"StartTimestamp": 123141245214,
"Sign":"58df2b36d1edb86c37f6f5cb363587b0",
"EndTimestamp": 123141245214,
"EcSiteId": "12",
"Nickname": "player1",
"PlayerProfit": 200000,
"SingleWalletType": 1,
"GameRecordId": "GR-234fsaf-faaf3r3",
"History": {}
}
```

| 欄位             | 屬性   | 說明                                                         |
| ---------------- | ------ | ------------------------------------------------------------ |
| PlayerId         | string | 遊戲平台玩家 Id                                              |
| Xid              | string | 請求唯一碼                                                   |
| GameId           | string | 遊戲 Id                                                      |
| BetAmount        | number | 此次下注金額(需要除 10000 才是真實金額，遊戲幣是 1:10000)    |
| WinAmount        | number | 此次贏的金額(需要除 10000 才是真實金額，遊戲幣是 1:10000)    |
| StartTimestamp   | number | 開始下注時間 unix 13 碼                                      |
| EndTimestamp     | number | 結束下注時間 unix 13 碼                                      |
| Sign             | string | MD5 效驗碼，請看 MD5 效驗說明                                |
| EcSiteId         | string | 貴方平台 Id 在遊戲平台的編號                                 |
| Nickname         | string | 玩家的暱稱                                                   |
| PlayerProfit     | number | 玩家此次贏的金額扣掉下注的金額的盈餘(需要除 10000 才是真實金額，遊戲幣是 1:10000) |
| SingleWalletType | number | 內部使用，對平台沒用到                                       |
| GameRecordId     | string | 此次遊戲注單編號                                             |
| History          | object | 每個遊戲的注單詳情                                           |



#### Response

- Body:
```json
{
"resCode": "0",
"data": {
      "afterBalance": 50000,
      "beforeBalance": 80000,
    }
}
```

| 欄位          | 屬性   | 說明                                                         |
| ------------- | ------ | ------------------------------------------------------------ |
| resCode       | string | 0: 成功、1: 失敗                                             |
| afterBalance  | number | 玩家餘額處理 PlayerProfit 後的金額(需要乘上 10000，再傳送過來) |
| beforeBalance | number | 玩家餘額處理 PlayerProfit 前的金額(需要乘上 10000，再傳送過來) |


## 第三方遊戲額外提供的接口

### 恢復失敗的下注

當玩家下注在第三方顯示失敗，但是在平台方有成功的時候，可以根據 `GameRecordId` 來強制要求第三方遊戲將此遊戲紀錄標記成下注成功，讓玩家的遊戲紀錄與平台方達成一致的結果

#### Reuqest

- Method: GET
- URL: `domain/SingleWalletGetRecord?playerId="123123"&startTime=12312321414&endTime=123341424`
- Headers： Content-Type:application/json

| params    | 屬性   | 說明                           |
| --------- | ------ | ------------------------------ |
| playerId  | string | 會員 Id                        |
| startTime | number | 搜尋下注開始的時間(非必要填選) |
| endTime   | number | 搜尋下注結束的時間(非必要填選) |

#### Response

- Body:
```json
{
"statusCode": "2000001",
"message": "SUCCESS",
"data": [失敗的下注資料陣列]
}
```

| 欄位       | 屬性   | 說明                              |
| ---------- | ------ | --------------------------------- |
| statusCode | string | "2000001": 成功、其他: 失敗原因。 |
| message    | string | API 訊息                          |
| data       | array  | 失敗的下注資料陣列                |

### 查詢失敗或是超時的下注

查詢因為 API 超時或是其他因素造成失敗的下注結果有哪些，可以進一步協助玩家將遊戲紀錄恢復

### Reuqest

- Method: POST
- URL: `domain/SingleWalletRestoreRecord`
- Headers： Content-Type:application/json
- Body:

```json
{
"gameRecordId": "12ff-345asf-fas32ff"
}
```

| 欄位         | 屬性   | 說明    |
| ------------ | ------ | ------- |
| gameRecordId | string | 注單 id |

#### Response

- Body:
```json
{
"statusCode": "2000001",
"message": "SUCCESS"
}
```

| 欄位       | 屬性   | 說明                              |
| ---------- | ------ | --------------------------------- |
| statusCode | string | "2000001": 成功、其他: 失敗原因。 |
| message    | string | API 訊息                          |
